//
//  BmiTableViewController.swift
//  BMICalculator
//
//  Created by Niko Holopainen on 26.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
//

import UIKit

class BmiTableViewController: UITableViewController, Delegator {

    var history = [Person]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! ViewController
        controller.persons = self
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bmiCell", for: indexPath)
        let person = history[indexPath.row]
        cell.textLabel?.text = "\(person.name)"
        cell.detailTextLabel?.text = "\(person.height)cm \(person.weight)kg - BMI:\(person.bmi)"
        return cell
    }
    
    func appendPerson(_ person: Person) {
        history.append(person)
    }
}
