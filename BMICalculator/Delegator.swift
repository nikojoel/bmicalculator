//
//  Delegator.swift
//  BMICalculator
//
//  Created by Niko Holopainen on 27.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
//

import Foundation

protocol Delegator {
    func appendPerson(_ person: Person)
}
