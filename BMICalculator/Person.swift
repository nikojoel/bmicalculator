//
//  Person.swift
//  BMICalculator
//
//  Created by Niko Holopainen on 26.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
//

import Foundation


class Person {
     let name: String
     let bmi: Double
     var weight: Double
     var height: Double
     private(set) var age = 20
     let minWeight = 45.0
     let minHeight = 145.0
     let maxAge = 100
     var professions = [String]()
     
     init?(_ name: String, _ weight: Double, _ height: Double) {
         if (weight < minWeight) {
             return nil
         } else if (height < minHeight) {
             return nil
         } else {
             self.weight = weight
             self.height = height
         }
         self.name = name
         self.bmi = ((Double(weight) / (pow(((Double(height) / 100)), 2))) * 10).rounded() / 10
     }
     
     func setHeight(_ height: Double) {
         if (height >= minHeight) {
             self.height = height
         } else {
             self.height = minHeight
         }
     }
     
     func setWeight(_ weight: Double) {
         if (weight >= minWeight) {
             self.weight = weight
         } else {
             self.weight = minWeight
         }
     }
     
     func setAge(_ newAge: Int) {
         if (newAge < age) {
             print("Cant get younger")
         } else if (newAge > maxAge) {
             print("Too old")
         } else {
             self.age = newAge
         }
     }
     
     func setProfession(_ prof: String) {
        if (professions.count < 5 && !professions.contains(prof)) {
             professions.append(prof)
         }
     }
 }
 
