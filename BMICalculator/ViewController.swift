//
//  ViewController.swift
//  BMICalculator
//
//  Created by Niko Holopainen on 26.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
// moikkakoikkaaaaaaaakkk§zxcxas§asdasdqweqwe

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var height: Double = 0.0
    var weight: Double = 0.0
    let weights = [Int](45...125)
    let heights = [Int](145...225)
    var persons: Delegator?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var bmiLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        pickerView.dataSource = self
        pickerView.delegate = self
        nameTextField.delegate = self
        height = 145.0
        weight = 45.0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return weights.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return "\(weights[row])kg"
        } else {
            return "\(heights[row])cm"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
         // use the row to get the selected row from the picker view
         // using the row extract the value from your datasource (array[row])
        if component == 0 {
            weight = Double(weights[row])
        } else {
            height = Double(heights[row])
        }
     }

    @IBAction func calcBMI(_ sender: UIButton) {
        if nameTextField.text == "" {
            let alert = UIAlertController(title: "Invalid input", message: "Enter an username", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            nameTextField.resignFirstResponder()
            guard let person = Person(nameTextField.text ?? "Unknown", weight, height) else {
                fatalError("Error while creating person")
            }
            persons?.appendPerson(person)
            bmiLabel.text = "\(person.bmi)"
            nameTextField.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacter = CharacterSet.letters
        let allowedCharacter1 = CharacterSet.whitespaces
        let characterSet = CharacterSet(charactersIn: string)

        return allowedCharacter.isSuperset(of: characterSet) || allowedCharacter1.isSuperset(of: characterSet)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (nameTextField.text == "") {
            return false
        } else {
            textField.resignFirstResponder()
            return true
        }
    }
}

