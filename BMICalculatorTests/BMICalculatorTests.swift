//
//  BMICalculatorTests.swift
//  BMICalculatorTests
//
//  Created by Niko Holopainen on 26.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
//

import XCTest
@testable import BMICalculator

class BMICalculatorTests: XCTestCase {
    
    func testWeight() {
        guard let person = Person("Niko", 80.1, 186.5) else {
            fatalError("Error at Person")
        }
        
        // Normal weight
        person.setWeight(75.7)
        XCTAssert(person.weight == 75.7, "Setting weight failed")
        
        // Normal height
        person.setHeight(190.5)
        XCTAssert(person.height == 190.5, "Setting weight failed")
        
        // Minimum weight
        person.setWeight(-100)
        XCTAssert(person.weight == 45.0, "Should not be able to be less than 45")
        
        // Minimum height
        person.setHeight(80)
        XCTAssert(person.height == 145.0, "Should not be able to be less than 145")
    }
    
    func testPersons() {
        guard let person1 = Person("Matti", 85.4, 190.2) else {
            fatalError("Error at person")
        }
        /* Fatal error
        guard let failedPerson = Person("Matti", -500, 190.2) else {
            fatalError("Error at person")
        }
        */
        
        // Set correct age
        person1.setAge(25)
        XCTAssert(person1.age == 25)
        
        // Set invalid age
        person1.setAge(-100)
        XCTAssert(person1.age == 25)
        
        // Set professions
        person1.setProfession("Doctor")
        person1.setProfession("Soldier")
        person1.setProfession("Minister")
        person1.setProfession("President")
        XCTAssert(person1.professions == ["Doctor", "Soldier", "Minister", "President"])
        
        // Maximum of 5 professions
        for _ in 1...10 {
            person1.setProfession("Looper")
        }
        
        XCTAssert(person1.professions == ["Doctor", "Soldier", "Minister", "President", "Looper"])
    }
}
