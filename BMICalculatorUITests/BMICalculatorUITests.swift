//
//  BMICalculatorUITests.swift
//  BMICalculatorUITests
//
//  Created by Niko Holopainen on 31.3.2020.
//  Copyright © 2020 Niko Holopainen. All rights reserved.
//

import XCTest

class BMICalculatorUITests: XCTestCase {
/*
    func testUI() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        // Navigate to bmi calculator
        app.navigationBars["History"].buttons["Add"].tap()
        
        // Focus on the input field
        app.textFields["Enter Name"].tap()
        
        // Type name niko
        let n = app.keys["n"]
        n.tap()
        
        let i = app.keys["i"]
        i.tap()
        
        let k = app.keys["k"]
        k.tap()
        
        let o = app.keys["o"]
        o.tap()
        
        // Close keyboard
        app.buttons["Return"].tap()
        
        // Select pickerwheel values
        app.pickerWheels.element(boundBy: 0).adjust(toPickerWheelValue: "85kg")
        
        app.pickerWheels.element(boundBy: 1).adjust(toPickerWheelValue: "187cm")
        
        // Calculate bmi
        app.buttons["Calculate"].tap()
        
        // Test bmi data
        let bmi = app.staticTexts["24.3"]
        XCTAssertEqual(bmi.label, "24.3")
        
        // Navigate to history
        app.navigationBars.buttons.element(boundBy: 0).tap()
        
        // Cell data
        let title = app.staticTexts.element(boundBy: 1)
        let subTitle = app.staticTexts.element(boundBy: 2)
        
        // Test cell data
        XCTAssertEqual(title.label, "niko")
        XCTAssertEqual(subTitle.label, "187.0cm 85.0kg - BMI:24.3")
    }
 */
}
